import ProofOfEmployment from './../../build/contracts/ProofOfEmployment.json';

var getContract = function(drizzle) {
  var contract = require('truffle-contract');
  var MyContract = contract({
    abi: ProofOfEmployment.abi
  });
  console.log(drizzle.web3.givenProvider);
  MyContract.setProvider(drizzle.web3.givenProvider);

  return MyContract.at(drizzle.contracts.ProofOfEmployment.address);
};

export { getContract };
