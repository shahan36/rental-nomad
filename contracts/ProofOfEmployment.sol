pragma solidity ^0.4.4;
import "./Federation.sol";


contract ProofOfEmployment is Federation {

    struct contractItem {
        address landlord;
        string ipfsFileHashId;
        bool status;
    }
    mapping(address => contractItem) public contracts;

    //event triggered when landlord initiates data request
    event dataRequestInitiated(address indexed landlord, address indexed employee);

    //event triggered when employee legitimizes a data request
    event employeeLegitimises(address indexed employee, address indexed landlord, string encryptedPartners);

    //event triggered when landlord uploads contract and sends it to employee for approval
    event approveContractRequest(address indexed landlord, address indexed employee, string fileHash);

    //event triggered when employee approves the contract
    event approveContractResponse(address indexed employee, address indexed landlord, string fileHash);

    //event triggered when landlords updates housing authority
    event updateHA(address indexed housingAuthority, address indexed landlord, string landlordName, address indexed employee, string employeeName, string fileHash);

    //event triggered when landlord initiates an employee verification request
    event verificationRequest(string encryptedEmployee, string encryptedRequirement, string persona, address thirdParty, address landlord);

    //event triggered when employee responds to the landlord's verification request
    event verificationResponse(string encryptedEmployee, string encryptedResponse, address landlord);

    //called at time of contract init to set the federation address
    constructor() public {
        federation = msg.sender;
    }

    //function called when landlord initiates employee data verification request to third party
    function RequestRequirementVerification(string encryptedEmployee, string encryptedRequirement, string persona, address thirdParty, address landlord) public {
        if (compareStrings(persona, "employer")) {
            require(checkEmployer(thirdParty));
        } else if (compareStrings(persona, "bank")) {
            require(checkBank(thirdParty));
        } else if (compareStrings(persona, "housingAuthority")) {
            require(checkHousingAuthority(thirdParty));
        } else if (compareStrings(persona, "insuranceProvider")) {
            require(checkInsuranceProvider(thirdParty));
        } else if (compareStrings(persona, "police")) {
            require(checkPolice(thirdParty));
        } else {
            revert();
        }

        emit verificationRequest(encryptedEmployee, encryptedRequirement, persona, thirdParty, landlord);
    }

    //function called when third party responds to landlord's request to verify employee details
    function VerifyRequirement(string encryptedEmployee, string encryptedResponse, address landlord) public only3rdParty(msg.sender) {
        emit verificationResponse(encryptedEmployee, encryptedResponse, landlord);
    }


    //function for landlord to initiate data request
    function VerifyData(address _employeeAddress) public {
        emit dataRequestInitiated(msg.sender, _employeeAddress);
    }

    // called from the Employee frontend that is watching for the event dataRequestInitiated
    function legitimzeDataRequest(address _landlord, string _encryptedPartners) public {
        emit employeeLegitimises(msg.sender, _landlord, _encryptedPartners);
    }

    //function called when landlord uploads contract
    function uploadContract(address _employeeAddress, string _fileHash) public {
        require(contracts[_employeeAddress].status == false);
        contracts[_employeeAddress].landlord = msg.sender;
        contracts[_employeeAddress].ipfsFileHashId = _fileHash;
        contracts[_employeeAddress].status = false;
        emit approveContractRequest(msg.sender, _employeeAddress, _fileHash);
    }

    //function for employee approval for the contract uploaded by landlord
    function approveContract(address _landlordAddress, string _fileHash) public {
        require(contracts[msg.sender].landlord == _landlordAddress);
        require(compareStrings(contracts[msg.sender].ipfsFileHashId, _fileHash));
        contracts[msg.sender].status = true;
        emit approveContractResponse(msg.sender, _landlordAddress, _fileHash);
    }

    function compareStrings (string a, string b) pure returns (bool){
        return keccak256(a) == keccak256(b);
    }

    //function called by the landlord to update the housing authority with the employee/tenant details and contract
    function updateHousingAuthority(string _name, address _employeeAddress, string _employeeName, address _housingAuthority, string _fileHash) public {
        require(housingAuthorities[_housingAuthority] == true);
        require(contracts[_employeeAddress].status == true);
        require(compareStrings(contracts[_employeeAddress].ipfsFileHashId, _fileHash));
        require(contracts[_employeeAddress].landlord == msg.sender);
        emit updateHA(_housingAuthority, msg.sender, _name, _employeeAddress, _employeeName, _fileHash);
    }

}