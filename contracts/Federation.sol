pragma solidity ^0.4.4;

contract Federation {

    //addrress for the federation node 
    //(currently in our case this would be the address of the node that deploys contract)
    address public federation;

    //3rd parties registered by federation
    mapping(address => bool) public employers;
    mapping(address => bool) public banks;
    mapping(address => bool) public housingAuthorities;
    mapping(address => bool) public insuranceProviders;
    mapping(address => bool) public polices;

    //function for federation to add a new employer
    function addEmployer(address _employer) public {
        require(msg.sender == federation);
        employers[_employer] = true;
    }

    //function for federation to add a new bank
    function addBank(address _bank) public {
        require(msg.sender == federation);
        banks[_bank] = true;
    }

    //function for federation to add a new bank
    function addHousingAuthority(address _housingAuthority) public {
        require(msg.sender == federation);
        housingAuthorities[_housingAuthority] = true;
    }

    //function for federation to add an insurancce provider
    function addInsuranceProvider(address _insuranceProvider) public {
        require(msg.sender == federation);
        insuranceProviders[_insuranceProvider] = true;
    }

    //function for federation to add police
    function addPolice(address _police) public {
        require(msg.sender == federation);
        polices[_police] = true;
    }

    //modifier to check if an employer has been registered with the federation
    modifier onlyEmployer(address _employer) {
        require(checkEmployer(_employer));
        _;
    }

    //modifier to check if a bank has been registered with the federation
    modifier onlyBank(address _bank) {
        require(checkBank(_bank));
        _;
    }

    //modifier to check if an housing authority has been registered with the federation
    modifier onlyHousingAuthority(address _housingAuthority) {
        require(checkHousingAuthority(_housingAuthority));
        _;
    }

    //modifier to check if an insurance provider has been registered with the federation
    modifier onlyInsuranceProvider(address _insuranceProvider) {
        require(checkInsuranceProvider(_insuranceProvider));
        _;
    }

    //modifier to check if police has been registered with the federation
    modifier onlyPolice(address _police) {
        require(checkPolice(_police));
        _;
    }

    // modifier called to verify if a third party exists
    modifier only3rdParty(address _actor) {
        bool found = false;
        if (checkEmployer(_actor))
            found = true;
        if(checkBank(_actor))
            found = true;
        if(checkHousingAuthority(_actor))
            found = true;
        if(checkInsuranceProvider(_actor))
            found = true;
        if(checkPolice(_actor))
            found = true;
        require(found == true);
        _;
    }

    //function call to check if an employer is registered
    function checkEmployer(address _employer) public view returns (bool) {
        return employers[_employer];
    }

    //function call to check if a bank is registered
    function checkBank(address _bank) public view returns (bool) {
        return banks[_bank];
    }

    //function call to check if an housing authority is registered
    function checkHousingAuthority(address _housingAuthority) public view returns (bool) {
        return housingAuthorities[_housingAuthority];
    }

    //function call to check if an insurance provider is registered
    function checkInsuranceProvider(address _insuranceProvider) public view returns (bool) {
        return insuranceProviders[_insuranceProvider];
    }

    //function call to check if a police is registered
    function checkPolice(address _police) public view returns (bool) {
        return polices[_police];
    }
}